package com.example.twdm.app.controller;

import com.example.twdm.app.model.NetworkInterfaceModel;
import com.example.twdm.app.network.NetworksInterface;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Map;

@RestController
public class NetworkController {

    public final NetworksInterface networksInterface;

    public NetworkController(NetworksInterface networksInterface) {
        this.networksInterface = networksInterface;
    }

    @GetMapping(value = "/getInterfaces")
    public Map<String, NetworkInterfaceModel> getInterfaces() throws SocketException, UnknownHostException {
        Map<String, NetworkInterfaceModel> interfaceModelMap = networksInterface.extractDetailsForInterfaces();
        System.out.println(interfaceModelMap.toString());
        return interfaceModelMap;
    }

    @GetMapping(value = "/getAllInterfaces")
    public Map<Integer, String> getAllInterfaces() throws SocketException {
        Map<Integer, String> interfaces = networksInterface.getAllInterfaces();
        System.out.println(interfaces.toString());
        return interfaces;
    }
}
