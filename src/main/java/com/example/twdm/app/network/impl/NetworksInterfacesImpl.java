package com.example.twdm.app.network.impl;

import com.example.twdm.app.model.NetworkInterfaceModel;
import com.example.twdm.app.network.NetworksInterface;
import org.springframework.stereotype.Service;

import java.net.*;
import java.util.*;

@Service
public class NetworksInterfacesImpl implements NetworksInterface {

    public HashMap<Integer, String> getAllInterfaces() throws SocketException {
        ArrayList<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
        HashMap<Integer, String> map = new HashMap<>();
        for(int i = 0; i< interfaces.size(); i++) {
            NetworkInterface ni = interfaces.get(i);
            map.put(i, ni.getDisplayName() + " [ " + ni.getName() + " ]");
        }

        return map;
    }

    public HashMap<String, NetworkInterfaceModel> extractDetailsForInterfaces() throws SocketException, UnknownHostException {
        ArrayList<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
        HashMap<String, NetworkInterfaceModel> map = new HashMap<>();

        for(int i = 0; i< interfaces.size(); i++) {
            NetworkInterface ni = interfaces.get(i);
            if (ni.isUp()) {
                NetworkInterfaceModel networkInterfaceModel = new NetworkInterfaceModel();
                networkInterfaceModel.setName(ni.getName());
                networkInterfaceModel.setDisplayName(ni.getDisplayName());
                networkInterfaceModel.setHardwareAddress(ni.getHardwareAddress());
                networkInterfaceModel.setParent(ni.getParent());
                networkInterfaceModel.setIndex(ni.getIndex());

                ArrayList<String> addresses = new ArrayList<>();
                for (InterfaceAddress addr : ni.getInterfaceAddresses()) {
                    addresses.add(addr.getAddress().toString());
                }

                networkInterfaceModel.setAddresses(addresses);
                networkInterfaceModel.setMtu(ni.getMTU());
                networkInterfaceModel.setSubInterfaces(Collections.list(ni.getSubInterfaces()));
                networkInterfaceModel.setLoopback(ni.isLoopback());
                networkInterfaceModel.setVirtual(ni.isVirtual());
                networkInterfaceModel.setPointToPoint(ni.isPointToPoint());
                networkInterfaceModel.setSupportsMulticast(ni.supportsMulticast());

                map.put(String.valueOf(i), networkInterfaceModel);
            }
        }

        return map;
    }
}
