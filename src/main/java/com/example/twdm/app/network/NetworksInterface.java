package com.example.twdm.app.network;

import com.example.twdm.app.model.NetworkInterfaceModel;

import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.HashMap;


public interface NetworksInterface {
    HashMap<String, NetworkInterfaceModel> extractDetailsForInterfaces() throws SocketException, UnknownHostException;
    HashMap<Integer, String> getAllInterfaces() throws SocketException;
}
