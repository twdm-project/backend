package com.example.twdm.app.model;

import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Arrays;

public class NetworkInterfaceModel {

    private String name;
    private String displayName;
    private byte[] hardwareAddress;
    private NetworkInterface parent;
    private int index;
    private ArrayList<String> addresses;
    private int mtu;
    private ArrayList<NetworkInterface> subInterfaces;
    private boolean loopback;
    private boolean virtual;
    private boolean pointToPoint;
    private boolean supportsMulticast;

    public NetworkInterfaceModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public byte[] getHardwareAddress() {
        return hardwareAddress;
    }

    public void setHardwareAddress(byte[] hardwareAddress) {
        this.hardwareAddress = hardwareAddress;
    }

    public NetworkInterface getParent() {
        return parent;
    }

    public void setParent(NetworkInterface parent) {
        this.parent = parent;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public ArrayList<String> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<String> addresses) {
        this.addresses = addresses;
    }

    public int getMtu() {
        return mtu;
    }

    public void setMtu(int mtu) {
        this.mtu = mtu;
    }

    public ArrayList<NetworkInterface> getSubInterfaces() {
        return subInterfaces;
    }

    public void setSubInterfaces(ArrayList<NetworkInterface> subInterfaces) {
        this.subInterfaces = subInterfaces;
    }

    public boolean isLoopback() {
        return loopback;
    }

    public void setLoopback(boolean loopback) {
        this.loopback = loopback;
    }

    public boolean isVirtual() {
        return virtual;
    }

    public void setVirtual(boolean virtual) {
        this.virtual = virtual;
    }

    public boolean isPointToPoint() {
        return pointToPoint;
    }

    public void setPointToPoint(boolean pointToPoint) {
        this.pointToPoint = pointToPoint;
    }

    public boolean isSupportsMulticast() {
        return supportsMulticast;
    }

    public void setSupportsMulticast(boolean supportsMulticast) {
        this.supportsMulticast = supportsMulticast;
    }

    @Override
    public String toString() {
        return "NetworkInterfaceModel{" +
                "name='" + name + '\'' +
                ", displayName='" + displayName + '\'' +
                ", hardwareAddress=" + Arrays.toString(hardwareAddress) +
                ", parent=" + parent +
                ", index=" + index +
                ", addresses=" + addresses +
                ", mtu=" + mtu +
                ", subInterfaces=" + subInterfaces +
                ", loopback=" + loopback +
                ", virtual=" + virtual +
                ", pointToPoint=" + pointToPoint +
                ", supportsMulticast=" + supportsMulticast +
                '}';
    }
}
