package com.example.twdm.app;

import com.example.twdm.app.network.NetworksInterface;
import com.example.twdm.app.network.impl.NetworksInterfacesImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.net.SocketException;
import java.net.UnknownHostException;

@SpringBootApplication
public class TwdmAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(TwdmAppApplication.class, args);
	}

}
